## Arduino Framework

Provides a framework for developing with Arduino. Developed to work with [Stino](https://github.com/Robot-Will/Stino), a Sublime Text plugin which turns Sublime Text into an Arduino IDE.

### Setup

1. Download and install [Stino](https://github.com/Robot-Will/Stino)
2. Clone this repo to a local directory using `git clone https://bitbucket.org/weyforth/arduino-framework Arduino`
3. Download the [Arduino software](http://arduino.cc/en/main/software), and drop the executable in the `ide` directory
4. Follow the setup instructions on the [Stino repo](https://github.com/Robot-Will/Stino)

### Working with projects

All sketch folders are stored in the `sketchbook` directory. The framework repository is setup so that the project directories themselves can be source controlled independently.

### Dependency Management

The framework provides git repository dependency management. To make use of this, within you projects root folder in `sketchbook`, create a file called `deps` and paste the github url of the repository you want to include. Then run `install.sh` from the terminal, and each repository will be downloaded into `sketchbook/libraries`