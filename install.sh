#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SKETCHDIR="${DIR}/sketchbook"
LIBDIR="${DIR}/sketchbook/libraries"

DEPS="${DIR}/deps"

echo "Installing"

if [ -d "${LIBDIR}" ]; then

    sudo rm -r "${LIBDIR}"

fi

mkdir "${LIBDIR}"
cd "${LIBDIR}"

for i in $(find "${SKETCHDIR}" -type d -depth 1 -not -name "hardware" -not -name "libraries"); do

    echo "  [${i}]"

    if [ -f "${i}/deps" ]; then

        for REPO in `cat "${i}/deps"`; do

            echo "    ${REPO}"
            git clone $REPO &>/dev/null

        done

    fi

done

echo "Updating"

for i in $(find "${LIBDIR}" -type d -depth 1); do

    cd "${i}"
    git pull origin &>/dev/null

done
